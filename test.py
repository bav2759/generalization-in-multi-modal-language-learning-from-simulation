#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 17:03:46 2020

@author: aaron
"""

import pandas
import datasets
import os.path
import sklearn.metrics
import torch, torchvision
import numpy as np
from torch.utils.data import DataLoader
from torch.nn import CrossEntropyLoss
from tqdm import tqdm
import json

def copy_state(state):
    if isinstance(state, tuple):
        return (state[0].data, state[1].data)
    else:
        return state.data
    
def test(model, data, criterion, loss, mask_configs, mask_probs, masking, k, batchsize, seq_length, shuffle, device, n_workers=0):
    if loss == "crossentropy":
        with torch.no_grad():
            return test_crossentropy(model, data, criterion, mask_configs, mask_probs, masking, k, 
                                    batchsize = batchsize, device = device, shuffle = shuffle, num_workers=n_workers)
    elif loss == "ctc":
        return test_ctc(model, data, criterion, k, batchsize, device)

def test_crossentropy(model, data, criterion, mask_configs, mask_probs, masking, k , batchsize, device, shuffle, num_workers):
    #data = datasets.SimpleDataset(directory)
    #with torch.no_grad():
    
    if shuffle:
        data.shuffle()
    dl = DataLoader(data, batch_size = batchsize, shuffle=False, pin_memory = True, num_workers=num_workers)
    data.gen_masks(mask_configs, mask_probs, masking)
    
    model.eval()
    #hidden_init = model.lstm.state0(1)
    hidden_init = model.state0(1)
    hidden = (hidden_init[0].to(device), hidden_init[1].to(device))

    running_loss = 0

    predictions = np.ndarray((len(data), k), dtype=int)
    ground_truth = np.ndarray((len(data),), dtype=int)
    c = 0
    #seq_c = 0
    
    
#    use_masks = len(mask_configs) > 1
    
#    if use_masks:
        #mask_predictions = np.ndarray((len(data), 1), dtype=int)
        #mask_ground_truth = np.ndarray((len(data),), dtype=int)
        #mask_hidden = model.state0(1)
        #mask_hidden = (mask_hidden[0].to(device), mask_hidden[1].to(device))
#        active_mask = mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)]
        #sen = []
#    else:
#        active_mask = mask_configs[0]
    
    #masks = list(itertools.product([0, 1], repeat=masksize))
    #mask_predictions = np.ndarray((len(masks), len(data)), dtype=int)
    #mask_hidden = [model.state0(1) for i in range(len(masks))]
    
    #for i in range(len(masks)):
    #    mask_hidden[i] = (mask_hidden[i][0].to(device), mask_hidden[i][1].to(device))

    #for batch in tqdm(dl, desc="Validation"):
    #hidden = hidden_init
    lang = [0 for i in range(len(data.words))]
    for (batch_img, batch_joints, _, batch_y, batch_mask, _) in tqdm(dl, desc="Validation"):
        X_img = batch_img.to(device, non_blocking = True)
        X_joint = batch_joints.to(device, non_blocking = True)
        #X_lang = batch_lang.to(device)
        y = batch_y.to(device, non_blocking = True)
        mask = batch_mask.to(device, non_blocking = False)
        #end = batch_end.to(device, non_blocking = False)
        
        for t in range(min(batchsize, len(batch_y))):
            #X1[t, -len(data.words):] = torch.tensor(lang)
            #X_lang
            
            hidden, output = model((X_img[t:t+1], X_joint[t:t+1], torch.tensor(lang, dtype=torch.float32, device=device).unsqueeze(0)), hidden, mask[t:t+1])#active_mask)
            output = output.squeeze(1)                    
                
            predictions[c] = output.squeeze().topk(k)[1].cpu().data
            label = y[t].topk(1)[1]
#            if use_masks:
#                if label.item() > 0 and active_mask[masking[label.item()] - 1] == 0:
#                    label[0] = 0
                #sen.append(label.item())
                
#                if end[t]:
            """
            if len(np.unique(sen)) > 2:
                print("Using mask:", active_mask)
                print("Sentence:", sen, "\n")
                print(data.index_map[c])
            sen = []
            """
#                    active_mask = mask_configs[np.random.choice(np.arange(len(mask_configs)), p=mask_probs)]
                    
            ground_truth[c] = label
                
            running_loss += criterion(output, label).item()
                
            lang = [(1 if i == predictions[c, 0] else 0) for i in range(len(lang))]
            
            c += 1
            #seq_c += 1
            
            #if seq_c == seqlength:
            #    seq_c = 0
            #    hidden = copy_state(hidden)
            
        
        #hidden = copy_state(hidden)
        del X_img, X_joint, y, mask
        
    loss_avg = running_loss / len(data)
#    if use_masks:
#        return loss_avg, predictions, ground_truth#, mask_predictions, mask_ground_truth
    return loss_avg, predictions, ground_truth#, 0, 0

def batch_test_crossentropy(model, data, criterion, mask_configs, mask_probs, masking, k , batchsize, seqlength, device, shuffle, num_workers):
    #data = datasets.SimpleDataset(directory)

    with torch.no_grad():
    
        if shuffle:
            data.shuffle()
        dl = DataLoader(data, batch_size = batchsize, shuffle=False, pin_memory = True, num_workers=num_workers)
        
        model.eval()
        #hidden_init = model.lstm.state0(1)
        hidden_init = model.state0(data.batches)
        hidden = (hidden_init[0].to(device), hidden_init[1].to(device))
    
        running_loss = 0
    
        predictions = np.ndarray((data.frames - 1, k), dtype=int)
        ground_truth = np.ndarray((data.frames - 1,), dtype=int)
        c = 0
        seq_c = 0
        
        use_masks = len(mask_configs) > 1
        
        if use_masks:
            #mask_predictions = np.ndarray((len(data), 1), dtype=int)
            #mask_ground_truth = np.ndarray((len(data),), dtype=int)
            #mask_hidden = model.state0(1)
            #mask_hidden = (mask_hidden[0].to(device), mask_hidden[1].to(device))
            #active_mask = mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)]
            active_masks = [mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)] for i in range(data.batches)]
            #sen = []
        else:
            #active_mask = mask_configs[0]
            active_masks = [mask_configs[0]] * data.batches
        active_masks = torch.tensor(active_masks, dtype=torch.float32, device=device)
        
        #masks = list(itertools.product([0, 1], repeat=masksize))
        #mask_predictions = np.ndarray((len(masks), len(data)), dtype=int)
        #mask_hidden = [model.state0(1) for i in range(len(masks))]
        
        #for i in range(len(masks)):
        #    mask_hidden[i] = (mask_hidden[i][0].to(device), mask_hidden[i][1].to(device))
    
        #for batch in tqdm(dl, desc="Validation"):
        #hidden = hidden_init
        #lang = [0 for i in range(len(data.words))]
        #lang = torch.zeros((data.batches, data.word_count), dtype=torch.float32, device=device)
        lang = torch.nn.functional.one_hot(torch.zeros((data.batches,), dtype=int), data.word_count).to(dtype=torch.float32, device=device)
        for (batch_img, batch_joints, _, batch_y, batch_end) in tqdm(dl, desc="Validation"):
            X_img = batch_img.to(device, non_blocking = True)
            X_joint = batch_joints.to(device, non_blocking = True)
            #X_lang = batch_lang.to(device)
            y = batch_y.to(device, non_blocking = True)
            end = batch_end.to(device, non_blocking = False)
            for t in range(min(batchsize, len(batch_y))):
                #X1[t, -len(data.words):] = torch.tensor(lang)
                #X_lang
                
                hidden, output = model((X_img[t], X_joint[t], lang), hidden, active_masks)
                output = output.squeeze(0)  
                  
                predictions[(data.batch_limits[:-1] + c) % data.batch_limits[-1]] = output.squeeze().topk(k)[1].cpu().data
                labels = y[t].topk(1)[1].squeeze(1)
                if use_masks:
                    for i in range(data.batches):
                        if labels[i].item() > 0 and active_masks[i][masking[labels[i].item()] - 1] == 0:
                            labels[i] = 0
                        #sen.append(label.item())
                        
                        if end[t, i]:
                            """
                            if len(np.unique(sen)) > 2:
                                print("Using mask:", active_mask)
                                print("Sentence:", sen, "\n")
                                print(data.index_map[c])
                            sen = []
                            """
                            active_masks[i] = torch.tensor(mask_configs[np.random.choice(np.arange(len(mask_configs)), p=mask_probs)])
                
                for i in range(data.batches):
                    ground_truth[(c + data.batch_limits[i]) % data.batch_limits[-1]] = labels[i].item()
                    #lang[i] = predictions[(data.batch_limits[:-1] + c) % data.batch_limits[-1]]
                    
                running_loss += criterion(output, labels).item()
                lang = torch.nn.functional.one_hot(torch.as_tensor(predictions[(data.batch_limits[:-1] + c) % data.batch_limits[-1], 0], dtype=int), num_classes = lang.shape[1]).to(dtype=torch.float32, device=device)
                #lang = [(1 if i == predictions[c, 0] else 0) for i in range(len(lang))]
                
                c += 1
                seq_c += 1
                
                if seq_c == seqlength:
                    seq_c = 0
                    hidden = copy_state(hidden)
                
            
            #hidden = copy_state(hidden)
        
    loss_avg = running_loss / c
    if use_masks:
        return loss_avg, predictions, ground_truth#, mask_predictions, mask_ground_truth
    return loss_avg, predictions, ground_truth#, 0, 0

def test_ctc(model, data, criterion, k, batchsize, device):
    dl = DataLoader(data, batch_size=batchsize, shuffle=False, num_workers=8)
    
    model.eval()
    hidden_init = model.state0(1)
    hidden_init = (hidden_init[0].to(device), hidden_init[1].to(device))
    
    running_loss = 0
    
    predictions = np.ndarray((len(data), k), dtype=int)
    ground_truth = np.ndarray((len(data),), dtype=int)
    c = 0
    
    ctc_inputs = []
    ctc_targets = []
    
    lang = [0 for i in range(len(data.words))]
    for (batch_img, batch_joints, batch_lang, batch_y) in tqdm(dl, desc="Validation"):
        hidden = hidden_init
        X_img = batch_img.to(device)
        X_joint = batch_joints.to(device)
        #X_lang = batch_lang.to(device)
        y = batch_y.to(device)

        for t in range(min(batchsize, len(batch_img))):
            #X1[t, -len(data.words):] = torch.tensor(lang)
            hidden, output = model((X_img[t], X_joint[t]), hidden)
            #output = output.squeeze(0)
            ctc_inputs.append(output)
            ctc_targets.append(y[t].topk(1)[1])
            
            predictions[c] = output.squeeze().topk(k)[1].cpu().data
            ground_truth[c] = y[t].argmax()
            lang = [(1 if i == predictions[c, 0] else 0) for i in range(len(lang))]
            c += 1
            if c in data.action_perimeters or c == len(data):
            #if not batch_mid_action[t]:
                #optimizer.zero_grad()
                #print("input size:", torch.cat(ctc_inputs).unsqueeze(1).shape)
                input = torch.cat(ctc_inputs).log_softmax(2)
                
                target = torch.tensor(ctc_targets, dtype=torch.float)
                target = target[target.nonzero().transpose(0,1)[0]] # unique_consecutive().unsqueeze(0)
                target = target.unique_consecutive()

                running_loss += criterion(input, target, (len(input),), (len(target),)).item()
                ctc_inputs.clear()
                ctc_targets.clear()
                
        hidden_init = copy_state(hidden)
    """
    input = torch.cat(ctc_inputs)
    target = torch.tensor(ctc_targets)
    target = target[target.nonzero().transpose(0,1)[0]]# .unique_consecutive().unsqueeze(0)
    target = target.unique_consecutive()
    running_loss += criterion(input, target, (len(input),), (len(target),)).item()
    print(criterion(input, target, (len(input),), (len(target),)).item())
    print(target)
    print("running loss:", running_loss)
    print(data.action_count)
    """
    loss_avg = running_loss / data.action_count
    return loss_avg, predictions, ground_truth

def evaluation(predictions, ground_truth, action_limits, labels, sentence_tracking = True, save_file = None):
    #import time
    #start = time.time()
    """
    if len(mask_neurons) > 0:
        masking = np.array(masking)
        replacement = {i : i for i in range(len(labels))}
        for i, n in enumerate(mask_neurons):
            if n == 0:
                for e in np.where(masking == i + 1)[0]:
                    replacement[e] = 0
        ground_truth = np.array([replacement[i] for i in ground_truth])
        
    if len(predictions.shape) == 1:
        predictions = np.reshape(predictions, (len(predictions), 1))
    """
    accuracy = np.sum(predictions[:, 0] == ground_truth) / len(ground_truth)
    topk_accuracy = np.sum(isin(ground_truth, predictions)) / len(ground_truth)
    
    n0_indicies = np.logical_or(ground_truth != 0, predictions[:, 0] != 0)
    non0_accuracy = np.sum(ground_truth[n0_indicies] == predictions[n0_indicies, 0]) / sum(n0_indicies)
    topk_non0_accuracy = sum(isin(ground_truth[n0_indicies], predictions[n0_indicies, :])) / sum(n0_indicies)
    
    sentence_hits = 0
    sentence_skip_blanks_hits = 0
    sentence_pairs = {}

    words = labels
    labels = np.arange(len(labels))
    
    confusion_matrix = sklearn.metrics.confusion_matrix(ground_truth, predictions[:, 0], labels=labels)
    
    for i in range(len(action_limits) - 1):
        gt = ground_truth[action_limits[i] : action_limits[i + 1]]
        pr = predictions[action_limits[i] : action_limits[i + 1], 0]
        
        if np.count_nonzero(gt) > 0:
            gt_l, gt_h = np.flatnonzero(gt)[[0, -1]] #Indicies of first and last non zero value in the ground truth
        else:
            gt_l = gt_h = 0
        
        if np.count_nonzero(pr) > 0:
            pr_l, pr_h = np.flatnonzero(pr)[[0, -1]] #Indicies of first and last non zero value in the prediction
        else:
            pr_l = pr_h = 0
        
        if np.array_equal(unique_consecutive(gt[gt_l : gt_h]), unique_consecutive(pr[pr_l : pr_h])):
            sentence_hits += 1
            
        if np.array_equal(unique_consecutive_ignore_0(gt[gt_l : gt_h]), unique_consecutive_ignore_0(pr[pr_l : pr_h])):
            sentence_skip_blanks_hits += 1
            
        if sentence_tracking:
            try:
                sentence_pairs[sentence_string(unique_consecutive(gt[gt_l : gt_h]), words) + " AS " + sentence_string(unique_consecutive(pr[pr_l : pr_h]), words)] += 1
            except KeyError:
                sentence_pairs[sentence_string(unique_consecutive(gt[gt_l : gt_h]), words) + " AS " + sentence_string(unique_consecutive(pr[pr_l : pr_h]), words)] = 1

    sentence_accuracy = sentence_hits / (len(action_limits) - 1)
    sentence_skip_blank_accuracy = sentence_skip_blanks_hits / (len(action_limits) - 1)

    dt = {}
    if sentence_tracking:
        GT = [0] * len(sentence_pairs)
        P = [0] * len(sentence_pairs)
        O = [0] * len(sentence_pairs)
        E = [False] * len(sentence_pairs)
        for i, e in enumerate(sentence_pairs):
            GT[i], P[i] = e.split(" AS ")
            O[i] = sentence_pairs[e]
            E[i] = GT[i] == P[i]
        
        df = pandas.DataFrame({"Ground Truth": GT, "Prediction": P, "Occurrences": O, "Correct": E})
                
        for g in sorted(np.unique(GT)):
            hits = 0
            misses = 0
            for i, p in enumerate(P):
                if g == GT[i]:
                    if g == p:
                        hits += O[i]
                    else:
                        misses += O[i]
            g_sup = hits + misses
            g_acc = hits / g_sup
            print(g, " - Accuracy:", g_acc, " Support:", g_sup)
            dt[g] = [g_acc, g_sup]

    else:
        df = None
        dt = {}
        
    if save_file is not None:
        df.to_csv(save_file + ".csv")
        with open(save_file + ".txt", "w") as f:
            for e in dt:
                f.write(str(e) + " - Accuracy: " + str(dt[e][0]) + "  Support: " + str(dt[e][1]) + "\n")
            f.write("\nFrame-wise accuracy: " + str(accuracy) + "\n")
            f.write("Frame-wise accuracy (ignoring 0s): " + str(non0_accuracy) + "\n")
            f.write("Frame-wise top-k accuracy: " + str(topk_accuracy) + "\n")
            f.write("Frame-wise top-k accuracy (ignoring 0s): " + str(topk_non0_accuracy) + "\n")
            f.write("Sentence-wise accuracy: " + str(sentence_accuracy) + "\n")
            f.write("Sentence-wise accuracy (skipping blanks inbetween words): " + str(sentence_skip_blank_accuracy) + str("\n"))

    return accuracy, topk_accuracy, non0_accuracy, topk_non0_accuracy, sentence_accuracy, sentence_skip_blank_accuracy, confusion_matrix, df, dt

def sentence_string(array, words):
    string = ""
    for i in array:
        if i != 0:
            if len(string) > 0:
                string += " "
            string += words[i]
            
    return string

def isin(a, b):
    return np.array([x in b [i] for i, x in enumerate(a)])

def unique_consecutive(array):
    unique = []
    if len(array > 0):
        buf = array[0]
        unique.append(buf)
        for i in range(1, len(array)):
            if buf != array[i]:
                buf = array[i]
                unique.append(buf)
    return np.array(unique)

def unique_consecutive_ignore_0(array):
    unique = []
    if len(array > 0):
        buf = array[0]
        if buf != 0:
            unique.append(buf)
        for i in range(1, len(array)):
            if buf != array[i] and array[i] != 0:
                buf = array[i]
                unique.append(buf)
    return np.array(unique)

if __name__ == "__main__":
    
    import argparse

    argparser = argparse.ArgumentParser(description="test")
    argparser.add_argument("data", help="Path to data set file")
    argparser.add_argument("model", help="Model path")
    argparser.add_argument("-params", "--training_params", default="")
    argparser.add_argument("-words", default="", help="Labels")
    argparser.add_argument("-mask", default="", help="Path to mask file. Default to the file specified in the params file")
    argparser.add_argument("-shuffle", action="store_true")
    argparser.add_argument("-save", default=None)
    argparser.add_argument("-batch", default=100, type=int)
    argparser.add_argument("-seq", default = None, type=int)
    argparser.add_argument("-workers", default=0, type=int)
    argparser.add_argument("-k", default = 2, type=int, help="top k")
    argparser.add_argument("-gpu", default = -1, type=int, help="device")
    
    argparser.set_defaults(shuffle=False)

    args = argparser.parse_args()
    
    params = argparse.Namespace()
    if args.training_params == "":
        print("No parameter file specified, trying to load parameters from model directory...")
        with open(os.path.join(os.path.dirname(args.model), "commandline_args.txt"), "r") as f:
            params.__dict__ = json.load(f)
        print("Using model parameters from file:", os.path.join(os.path.dirname(args.model), "commandline_args.txt"))
    else:
        with open(args.training_params, "r") as f:
            params.__dict__ = json.load(f)
            
    with open(args.data, "r") as file:
        test_paths = file.read().strip().splitlines()
        for i, line in enumerate(test_paths):
            print("Test set 1 " + str(i) + ":", line)
    del file
            
    f = torchvision.transforms.ToTensor()
    data = datasets.ActionDataset(test_paths[0], args.words, transf=f)

    if args.mask == "":
        mask_pth = params.mask
    else:
        mask_pth = args.mask

    if mask_pth != "":
        masking = np.loadtxt(mask_pth, dtype=int)
        masksize = np.count_nonzero(np.unique(masking))
        default_mask = (1,) * masksize
    else:
        masking = None
        masksize = 0
        default_mask = ()
    default_mask = torch.tensor(default_mask, dtype=torch.float32)

    if args.gpu == -1:
        device = torch.device("cpu")
    else:
        device = torch.device("cuda:" + str(args.gpu))
        
    if args.seq == None:
        SEQUENCE_SIZE = params.seq_length
    else:
        SEQUENCE_SIZE = args.seq
    #torch.set_default_tensor_type(torch.DoubleTensor)
    
    from train import BatchModel, Model
    
    model = Model(layers = params.hidden_layers, hiddensize = params.hidden_size, outputsize = data.word_count, num_words = data.word_count,
                  imagesize = data[0][0].shape, vision = params.vision_model, vision_layer = params.vision_layer,
                  masksize = masksize, fc_output_size = params.dense_size, joints = 6, device = device)
    
    """
    model = Model(layers = params.hidden_layers, hiddensize = params.hidden_size, outputsize = data.word_count, num_words = data.word_count,
                  imagesize = data[0][0].shape, vision = params.vision_model, vision_layer = params.vision_layer,
                  masksize = masksize, fc_output_size = params.dense_size, joints = 6, device = device)
    """
    #model = Model(1, 256, len(data.words), words=(len(data.words)), fc_output_size=32, imagesize=data[0][0].shape, masking=2).to(device)
    #model = torch.load(os.path.join(args.model), map_location=torch.device(device))
    model.load_state_dict(torch.load(args.model, map_location=torch.device(device)))
    
    criterion = CrossEntropyLoss()
    
    csv_path = args.save
    
    for i, path in enumerate(test_paths):
        print("Testing dataset:", path)
        save_path = csv_path + "_" + str(i) if csv_path is not None else None
        print("Saving results to:", save_path)
        test_data = datasets.ActionDataset(path, args.words, transf = f)

        loss, predictions, ground_truth = test(model=model, data=test_data, criterion=criterion, 
                                               mask_configs=[default_mask], mask_probs=[1], 
                                               masking = masking, loss = params.loss, k=args.k,
                                               batchsize = args.batch, seq_length= SEQUENCE_SIZE,
                                               shuffle = args.shuffle, device = device, 
                                               n_workers=args.workers)
        
        accuracy, topk_accuracy, n0_accuracy, n0_topk_accuracy, sen_accuracy, sen_n0_accuracy, cm, s, _ = evaluation(predictions, ground_truth, test_data.action_perimeters, labels=test_data.words, 
                                                                                                                          sentence_tracking=True, save_file = save_path)
        
        print("Loss:", loss)
        print("Accuracy:", accuracy)
        print("topk accuracy:", topk_accuracy)
        print("Accuracy (ignoring cases where truth and prediction are 0):", n0_accuracy)
        print("Top k accuracy (ignoring cases where truth and prediction are 0):", n0_topk_accuracy)
        print("Sentence-wise accuracy:", sen_accuracy)
        print("Sentence-wise accuracy (ignoring intermediate blanks):", sen_n0_accuracy)
        print("Word confusion matrix:")
        print(cm)
        print("\n")
        print(s)
    """
    loss, predictions, ground_truth = test(model, data, criterion, mask_configs = [default_mask], mask_probs = [1],
                                           masking = masking, loss="crossentropy", k=args.k, batchsize=args.batch,
                                           seq_length = SEQUENCE_SIZE, shuffle = args.shuffle, device=device,
                                           n_workers=args.workers)

    accuracy, topk_accuracy, n0_accuracy, n0_topk_accuracy, sen_accuracy, sen_n0_accuracy, cm, s, _ = evaluation(predictions, ground_truth, data.action_perimeters, labels=data.words, sentence_tracking=True, save_file=args.save_file)

    print("Loss:", loss)
    print("Accuracy:", accuracy)
    print("topk accuracy:", topk_accuracy)
    print("Accuracy (ignoring cases where truth and prediction are 0):", n0_accuracy)
    print("Top k accuracy (ignoring cases where truth and prediction are 0):", n0_topk_accuracy)
    print("Sentence-wise accuracy:", sen_accuracy)
    print("Sentence-wise accuracy (ignoring intermediate blanks):", sen_n0_accuracy)
    print("Word confusion matrix:")
    print(cm)
    print("\n")
    print(s)
    """