#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 23 22:38:56 2020

@author: aaron
"""

import torch
import torchvision
from torchvision import transforms



#use_cuda = torch.cuda.is_available()
#device = torch.device("cuda" if use_cuda else "cpu")
"""
model = torch.nn.Sequential(*list(torchvision.models.vgg16(pretrained = True).children())[0]).to(device)


model = models.Darknet("yolov3.cfg", 128).to(device)

extractor_model = models.Darknet("yolov3.cfg", 128)
model.load_darknet_weights("yolov3.weights")

#model = torch.nn.Sequential(*list(next(model.children()).children())[:95]).to(device)
"""


#if use_cuda:
 #   model.cuda()

#import ssl

#ssl._create_default_https_context = ssl._create_unverified_context
 
class VisionModule(torch.nn.Module):
    def __init__(self, model = "vgg16", layer = None, imagesize = 128, device=None):
        super(VisionModule, self).__init__()
        if model == "vgg16":
            if layer is None:
                self.model = torchvision.models.vgg16(pretrained=True).to(device)
            else:
                self.model = torch.nn.Sequential(torchvision.models.vgg16(pretrained=True).features[:layer+1]).to(device)

        elif model == "yolo":
            import sys
            sys.path.append("../PyTorch-YOLOv3")
            import models
            self.model = models.Darknet("yolov3.cfg", imagesize, layer=layer).to(device)
            
        elif model == "resnet":
            """
            if layer is None:
                self.model = torchvision.models.resnet101(pretrained=True).to(device)
            else:
            """
            resnet = torchvision.models.resnet50(pretrained=True).to(device)
            print("Using ResNet101 - layer[:-3]")
            self.model = torch.nn.Sequential(*list(resnet.children())[:-3]).to(device)
            del resnet
            
        for param in self.model.parameters():
            param.requires_grad = False
        self.model.eval()

    def forward(self, x):
        with torch.no_grad():
            return self.model(x)

class FeatureExtractor():
    
    def __init__(self, model = "vgg16", device=None, layer = None, imagesize = 128):
        self.device = device
        if model == "vgg16":
            self.transf = transforms.Compose([
                    transforms.ToTensor(),
                    transforms.Resize(224),
                    transforms.CenterCrop(224),
                    #transforms.ToTensor(),
                    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
            if layer is not None:
                #self.model = torch.nn.Sequential(*list(torchvision.models.vgg16(pretrained = True).modules())[:layer])
                self.model = torch.nn.Sequential(torchvision.models.vgg16(pretrained=True).features[:layer+1])
            else:
                self.model = torchvision.models.vgg16(pretrained=True)
        elif model == "yolo":
            self.transf = transforms.ToTensor()
            if layer is not None:
                self.model = models.Darknet("yolov3.cfg", imagesize, layer=layer).to(device)
            else:
                self.model = models.Darknet("yolov3.cfg", imagesize).to(device)
        #self.model.float()
        self.model.eval()
        for param in self.model.parameters():
            param.requires_grad = False

    def extract(self, image):
        with torch.no_grad():
            #img = PIL.Image.fromarray(image, "RGB")
            img_t = self.transf(image)
            #output = model(Variable(img_t.unsqueeze(0)))
            return self.model(img_t.unsqueeze(0).to(self.device))
        
    #def extract_PIL(self, pillow):
     #   with torch.no_grad():
      #      img_t = self.transf(pillow)
       #     return self.model(img_t.unsqueeze(0).to(self.device)

        

transform = transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225])])
    
yolo_transforms = transforms.ToTensor()
"""
def feature_extraction(image):
    with torch.no_grad():
        img = PIL.Image.fromarray(image, "RGB")
        img_t = transform(img)
        batch_t = torch.unsqueeze(img_t, 0)
        if use_cuda:
            batch_t = batch_t.cuda()
        output = model(batch_t)
        return output.cpu().detach()
"""



if __name__ == "__main__":
    model = torch.nn.Sequential(VisionModule())
    import PIL
    img = PIL.Image.open("data/frame_000000.png")
    img = yolo_transforms()
    output = model(img.unsqueeze(0))
