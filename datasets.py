#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 14:44:34 2020

@author: aaron
"""

import h5py, torch, PIL, glob
import numpy as np
import os.path
from torch.utils.data import Dataset
import features
#import pandas

class BatchActionDataset(Dataset):
    def __init__(self, path_file, batches, words_file = "", sequence_pattern = "sequence_*", file_pattern = "frame_{:06d}", transf = lambda x: x):
        self.action_dirs = []
        self.batches = batches
        with open(path_file) as file:
            for line in file:
                seq = glob.glob(os.path.join(line.strip(), sequence_pattern))
                seq.sort()
                self.action_dirs += seq
        #self.action_dirs = glob.glob(os.path.join(directory, sequence_pattern))
        #self.action_dirs.sort()
        self.pattern = file_pattern
        self.transf = transf

        self.action_count = len(self.action_dirs)
        self.ordering = np.arange(self.action_count)
        
        self.frames = 0
        for i in range(self.action_count):
            with open(os.path.join(self.action_dirs[i], "action.info"), "r") as file:
                self.frames += int(file.readline().strip().split(" ")[1])
        
        #with open(os.path.join(words_file, "data.info"), "r") as file:
        if words_file == "":
            words_file = os.path.join(line.strip(), "data.info")
            print("Using words from file:", words_file)
            with open(words_file, "r") as file:
                line = file.readline()
                while not line.startswith("Word list:"):
                    line = file.readline()
                self.words = line.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
        else:
            with open(words_file, "r") as file:
                self.words = file.read().strip().split("\n")
        print("Word list:", self.words)
        self.word_count = len(self.words)
            
        self.index_map = np.ndarray((self.frames, 2), dtype = int)
        self.update_mapping()
        
        self.imagesize = self.__getitem__(0)[0].shape #Assuming equal image size for all images in dataset
        
        
    def shuffle(self):
        np.random.shuffle(self.ordering)
        self.update_mapping()

        
    def update_mapping(self):
        f = 0
        for i in self.ordering:
            with open(os.path.join(self.action_dirs[i], "action.info"), "r") as file:
                for j in range(int(file.readline().strip().split(" ")[1])):
                    self.index_map[f, 0] = i
                    self.index_map[f, 1] = j
                    f += 1
        self.action_perimeters = np.concatenate((sorted(np.unique(self.index_map[:, 0], return_index=True)[1]), [self.index_map.shape[0] - 1]))
        
        self.batch_limits = np.zeros((self.batches + 1), dtype=int)
        for i in range(1, self.batches):
            self.batch_limits[i] = i * self.frames // self.batches - self.index_map[i * self.frames // self.batches, 1]
        self.batch_limits[-1] = self.frames - 1
        
        self.max_batch_frames = 0
        for i in range(self.batch_limits.__len__() - 1):
            self.max_batch_frames = max(self.max_batch_frames, self.batch_limits[i + 1] - self.batch_limits[i])
        
    def __len__(self):
        return self.max_batch_frames
    
    def get(self, idx, batch):
        idx += self.batch_limits[batch]
        idx %= self.frames - 1
        action_dir = self.action_dirs[self.index_map[idx, 0]]
        file_name = self.pattern.format(self.index_map[idx, 1])
        next_action_dir = self.action_dirs[self.index_map[idx + 1, 0]]
        next_file_name = self.pattern.format(self.index_map[idx + 1, 1])
        img = PIL.Image.open(os.path.join(action_dir, file_name + ".png"))
        img_t = self.transf(img)
        vec = torch.as_tensor(np.loadtxt(os.path.join(action_dir, file_name + ".txt"), dtype=np.float32))
        y = torch.as_tensor(np.loadtxt(os.path.join(next_action_dir, next_file_name + ".txt"), dtype=np.float32))[-len(self.words):]
        end_of_action = (self.index_map[idx, 0] != self.index_map[idx + 1, 0]) or (len(self.index_map) == idx + 1)
        #print("\nidx:", idx, "vec:", vec, "y:", y)
        joints = vec[:-self.word_count]
        language = vec[-self.word_count:]
        return img_t, joints, language, y, torch.tensor(end_of_action)
    
    def __getitem__(self, idx):
        items = [self.get(idx, i) for i in range(self.batches)]
        
        img = torch.stack(tuple(items[i][0] for i in range(len(items))))
        joints = torch.stack(tuple(items[i][1] for i in range(len(items))))
        lang = torch.stack(tuple(items[i][2] for i in range(len(items))))
        y = torch.stack(tuple(items[i][3] for i in range(len(items))))
        end = torch.stack(tuple(items[i][4] for i in range(len(items))))
        
        return img, joints, lang, y, end

class ActionDataset(Dataset):
    def __init__(self, path_file, words_file = "", sequence_pattern = "sequence_*", file_pattern = "frame_{:06d}", transf = lambda x: x):
        self.action_dirs = []
        self.action_dir_sizes = []
        with open(path_file) as file:
            for line in file:
                seq = glob.glob(os.path.join(line.strip(), sequence_pattern))
                seq.sort()
                self.action_dirs += seq
                
        if words_file == "":
            words_file = os.path.join(line.strip(), "data.info")
            print("Using words from file:", words_file)
            with open(words_file, "r") as file:
                #self.frames = int(file.readline().strip().split(" ")[-1])
                line = file.readline()
                while not line.startswith("Word list:"):
                    line = file.readline()
                self.words = line.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
        else:
            with open(words_file, "r") as file:
                self.words = file.read().strip().split("\n")
        #self.action_dirs = glob.glob(os.path.join(directory, sequence_pattern))
        #self.action_dirs.sort()
        self.pattern = file_pattern
        self.transf = transf

        self.action_count = len(self.action_dirs)
        self.ordering = np.arange(self.action_count)
        
        self.frames = 0
        self.action_dir_sizes = [0] * len(self.action_dirs)
        for i in range(self.action_count):
            with open(os.path.join(self.action_dirs[i], "action.info"), "r") as file:
                #self.frames += int(file.readline().strip().split(" ")[1])
                for line in file.readlines():
                    if line.startswith("Frames:"):
                        frames = int(line.split(" ")[1].strip("[,"))
                        self.frames += frames
                        self.action_dir_sizes[i] = frames
                        
        
        #with open(os.path.join(words_file, "data.info"), "r") as file:

        print("Word list:", self.words)
        self.word_count = len(self.words)
            
        self.index_map = np.ndarray((self.frames, 2), dtype = int)
        self.update_mapping()
        
        self.gen_masks([torch.tensor([], requires_grad=False)], [1], [0] * self.word_count)

        
        self.imagesize = self.__getitem__(0)[0].shape #Assuming equal image size for all images in dataset
        
    def shuffle(self):
        np.random.shuffle(self.ordering)
        self.update_mapping()
        
    def gen_masks(self, configs, probs, mapping):
        self.mask_configs = configs
        self.mask_mapping = mapping
        self.masks = np.random.choice(np.arange(len(configs)), len(self.action_dirs), p=probs)
        
    def apply_mask(self, language_vec, action_idx):
        mask = self.mask_configs[self.masks[action_idx]]
        for i, v in enumerate(mask):
            if v.item() == 0 and self.mask_mapping[language_vec.argmax()] - 1 == i:
                language_vec = torch.zeros_like(language_vec, requires_grad=False)
                language_vec[0] = 1
        return language_vec
        
    def update_mapping(self):
        f = 0
        for i in self.ordering:
            #with open(os.path.join(self.action_dirs[i], "action.info"), "r") as file:
            for j in range(self.action_dir_sizes[i]):
                #for j in range(int(file.readline().strip().split(" ")[1])):
                self.index_map[f, 0] = i
                self.index_map[f, 1] = j
                f += 1
        self.action_perimeters = np.concatenate((sorted(np.unique(self.index_map[:, 0], return_index=True)[1]), [self.index_map.shape[0] - 1]))
        
    def __len__(self):
        return self.frames - 1
    
    def __getitem__(self, idx):
        action_dir = self.action_dirs[self.index_map[idx, 0]]
        file_name = self.pattern.format(self.index_map[idx, 1])
        next_action_dir = self.action_dirs[self.index_map[idx + 1, 0]]
        next_file_name = self.pattern.format(self.index_map[idx + 1, 1])
        img = PIL.Image.open(os.path.join(action_dir, file_name + ".png"))
        img_t = self.transf(img)
        vec = torch.as_tensor(np.loadtxt(os.path.join(action_dir, file_name + ".txt"), dtype=np.float32))
        y = torch.as_tensor(np.loadtxt(os.path.join(next_action_dir, next_file_name + ".txt"), dtype=np.float32))[-len(self.words):]
        end_of_action = (self.index_map[idx, 0] != self.index_map[idx + 1, 0]) or (len(self) == idx + 1)
        #print("\nidx:", idx, "vec:", vec, "y:", y)
        joints = vec[:-self.word_count]
        language = vec[-self.word_count:]
        language = self.apply_mask(language, self.index_map[idx, 0])
        y = self.apply_mask(y, self.index_map[idx + 1, 0])
        mask = self.mask_configs[self.masks[self.index_map[idx, 0]]].detach()
        return img_t, joints, language, y, mask, end_of_action

        
        
class ImageDataset(Dataset):
    def __init__(self, directory, sequence_pattern = "sequence_{:04d}", file_pattern = "frame_{:06d}", transf = None):
        self.transf = transf
        self.dir = directory
        self.subdir_pattern = sequence_pattern
        self.pattern = file_pattern
        file = open(os.path.join(directory, "data.info"))
        line = file.readline()
        self.frames = int(line.strip().split(" ")[-1])
        line = file.readline()
        self.words = line.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
        
        self.sequence_mapping = np.ndarray((self.frames, 2), dtype=int)
        s = 0
        f = 0
        while os.path.exists(os.path.join(self.dir, self.subdir_pattern.format(s))) and f < self.frames:
            for i in range(np.loadtxt(os.path.join(self.dir, self.subdir_pattern.format(s), "action.info"), dtype=int)[1]):
                self.sequence_mapping[f, 0] = s
                self.sequence_mapping[f, 1] = i
                f += 1
            s += 1
            

        self.imagesize = self.__getitem__(0)[0].size
        
    def __len__(self):
        return self.frames - 1
    
    def __getitem__(self, idx):
        s, f = self.sequence_mapping[idx]
        s_n, f_n = self.sequence_mapping[idx + 1]
        img = PIL.Image.open(os.path.join(self.dir, self.subdir_pattern.format(s), self.pattern.format(idx) + ".png"))
        img_t = self.transf(img)
        vec = torch.as_tensor(np.loadtxt(os.path.join(self.dir, self.subdir_pattern.format(s), self.pattern.format(idx) + ".txt"), dtype=np.float32))
        y = torch.as_tensor(np.loadtxt(os.path.join(self.dir, self.subdir_pattern.format(s_n), self.pattern.format(idx + 1) + ".txt"), dtype=np.float32))[-len(self.words):]
        return img_t, vec, y

class SimpleDataset(Dataset):
    def __init__(self, directory, file_pattern = "frame_{:06d}"):
        self.dir = directory
        f = open(os.path.join(directory, "data.info"))
        l = f.readline()
        self.frames = int(l.strip().split(" ")[-1])
        l = f.readline()
        self.words = l.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
    #return frames, words
    
        self.input_size = 6 + 4 * 3 + len(self.words)
        self.output_size = len(self.words)

        self.data = np.ndarray((self.frames-1, self.input_size + self.output_size))
        for i in range(self.frames-1):
            self.data[i] = np.concatenate((np.loadtxt(os.path.join(self.dir, file_pattern.format(i) + ".txt")), np.loadtxt(os.path.join(self.dir, file_pattern.format(i+1) + ".txt"))[-self.output_size:]))
        self.data = torch.tensor(self.data)
        
    def __len__(self):
        return self.data.shape[0]
    
    def __getitem__(self, idx):
        return self.data[idx]
    

    
class VisionDataset(Dataset):
    def __init__(self, directory, device=None, file_pattern = "frame_{:06d}", imagesize = 128):
        self.device = device
        self.dir = directory
        self.pattern = file_pattern
        file = open(os.path.join(directory, "data.info"))
        line = file.readline()
        self.frames = int(line.strip().split(" ")[-1])
        line = file.readline()
        self.words = line.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
        
        self.feature_extractor = features.FeatureExtractor(model="yolo", device=self.device, layer=82)
        self.input_size = self.feature_extractor.extract(PIL.Image.open(os.path.join(directory, file_pattern.format(0) + ".png"))).numel() + len(self.words) + 6
#        self.input_size = 6 + imagesize * imagesize * 3 + len(self.words)
        self.output_size = len(self.words)
        
    def __len__(self):
        return self.frames - 1
    
    def __getitem__(self, idx):
        data = torch.empty(self.input_size + self.output_size, device=self.device)
        #img = cv2.imread(os.path.join(self.dir, self.pattern.format(idx) + ".png"))
        #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = PIL.Image.open(os.path.join(self.dir, self.pattern.format(idx) + ".png"))
        img_features = self.feature_extractor.extract(img)
        #img = cv2.normalize(img, None, alpha = 0, beta = 1, norm_type = cv2.NORM_MINMAX, dtype=cv2.CV_64F)
        #img_features = np.array(self.feature_extractor.extract(img))
        data[:img_features.numel()] = img_features.flatten()
        data[img_features.numel():-self.output_size] = torch.as_tensor(
                np.loadtxt(os.path.join(self.dir, self.pattern.format(idx) + ".txt")), dtype=np.float32, device=self.device)
        data[-self.output_size:] = torch.as_tensor(
                np.loadtxt(os.path.join(self.dir, self.pattern.format(idx + 1) + ".txt"))[-self.output_size:], dtype=np.float32, device=self.device)
        return data
    
"""
Now using the feature extraction module instead of original images
"""
class VisionDatasetPreload(Dataset):
    def __init__(self, directory, file_pattern = "frame_{:06d}", imagesize = 128, cuda=False):
        f = open(os.path.join(directory, "data.info"))
        l = f.readline()
        self.frames = int(l.strip().split(" ")[-1])
        l = f.readline()
        self.words = l.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
        self.feature_extractor = features.FeatureExtractor(model="vgg16", layer=31)
        self.input_size = 6 + self.feature_extractor.extract(np.zeros((imagesize, imagesize, 3))).shape.numel() + len(self.words)
        #self.input_size = 6 + imagesize * imagesize * 3 + len(self.words)
        self.output_size = len(self.words)
        self.data = np.ndarray((self.frames - 1, self.input_size + self.output_size))
        #self.data = torch.zeros((self.frames - 1, self.input_size + self.output_size))
        for i in range(self.frames - 1):
            img = cv2.imread(os.path.join(directory, file_pattern.format(i) + ".png")) #Leads Image in BGRA image forma
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) # Convert into RGB image
            #img = cv2.normalize(img, None, alpha = 0, beta = 1, norm_type = cv2.NORM_MINMAX, dtype=cv2.CV_64F)
            img_features = np.array(self.feature_extractor.extract(img))
            self.data[i, :img_features.size] = img_features.flatten()
            self.data[i, img_features.size:-self.output_size] = np.loadtxt(os.path.join(directory, file_pattern.format(i) + ".txt"))
            self.data[i, -self.output_size:] = np.loadtxt(os.path.join(directory, file_pattern.format(i + 1) + ".txt"))[-self.output_size:]
        self.data = torch.tensor(self.data)
        
    def __len__(self):
        return self.data.shape[0]
    
    def __getitem__(self, idx):
        return self.data[idx]
        
class HDF5Dataset(Dataset):
    def __init__(self, directory, file_pattern = "frame_{:06d}", metafile="data.info", storagefile="data.hdf5", reload=True):
        super(HDF5Dataset, self).__init__()
        
        if reload or not os.path.exists(os.path.join(directory, storagefile)):
            file = h5py.File(os.path.join(directory, storagefile), "w")

            with open(os.path.join(directory, metafile)) as f:
                l = f.readline()
                frames = int(l.strip().split(" ")[-1])
                l = f.readline()
                self.words = l.strip().split("[")[-1].strip("]").replace("'", "").split(", ")
            
            self.feature_extractor = features.FeatureExtractor(model="vgg16", layer=17)
            f0 = PIL.Image.open(os.path.join(directory, file_pattern.format(0) + ".png"))
            vision_size = self.feature_extractor.extract_PIL(f0).numel()
            self.inputsize = vision_size + 6 + len(self.words)
            self.outputsize = len(self.words)
            
            file.create_dataset("data", shape=(frames-1, self.inputsize + self.outputsize), dtype="f")
            file.attrs["words"] = self.words
            
            vis_data = file["data"]
            for i in range(frames - 1):
                frame = PIL.Image.open(os.path.join(directory, file_pattern.format(i) + ".png"))
                vis_data[i, :vision_size] = self.feature_extractor.extract_PIL(frame).flatten()
                vis_data[i, vision_size : -self.outputsize] = np.loadtxt(os.path.join(directory, file_pattern.format(i) + ".txt"))
                vis_data[i, -self.outputsize:] = np.loadtxt(os.path.join(directory, file_pattern.format(i + 1) + ".txt"))[-self.outputsize:]
            
            file.close()
            
        file = h5py.File(os.path.join(directory, storagefile), "r+")
        self.data = file["data"]
        self.words = file["words"]
        
    def __len__(self):
        return self.data.shape[0]
    
    def __getitem__(self, idx):
        return self.data[idx]
