#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 16:46:14 2020

@author: aaron
"""

import datasets, os.path, sys, time, datetime
import pickle, json
import torch
import numpy as np
from tqdm import tqdm
from torch.utils.data import DataLoader
from torch.autograd import Variable
#from usn_model import StackedLSTM, mLSTM
from torch.nn import CrossEntropyLoss, CTCLoss
from torch.optim import Adam, AdamW
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.tensorboard import SummaryWriter

import torchvision.transforms
import test, features

class MaskScheduler:
    """
    Adjusts the probabilities of the masks over the duration of the training
    
    (ndarray) probabilities:
        Array of shape (S, N), where N is the number of masks and
            and S is the number of phases
    
    (int) patience:
        after how many epochs the next set of probabilities should be used
        
    (string) mode [= "max"]:
        "max" if the monitored metric is to be maximized (e.g. Accuracy),
        "min" if it is to be minimized (e.g. Loss)
        
    (optimizer) optimizer [= None]:
        The optimizer of which the learning rate should be changed
        
    (int) lr [= 1e-3]:
    """
    
    def __init__(self, patience, probabilities, mode="max", optimizer = None, lr = 1e-3):
        self.patience = patience
        self.p = 0
        self.prob_list = probabilities
        self.probs = self.prob_list[0]
        self.idx = 0
        self.end = False
        self.optim = optimizer
        self.lr = lr
        self.reset_lr = self.optim is not None
        self.mode = mode
        if self.mode == "min":
            self.checkpoint = np.math.inf
        elif self.mode == "max":
            self.checkpoint = -np.math.inf
        self.best = [self.checkpoint] * len(probabilities)
    
    def step(self, metric):
        if self.mode == "max":
            if self.checkpoint < metric:
                self.p = 0
                self.checkpoint = metric
            else:
                self.p += 1
                
        elif self.mode == "min":
            if self.checkpoint > metric:
                self.p = 0
                self.checkpoint = metric
            else:
                self.p += 1
                
        self.best[self.idx] = self.checkpoint

        if not self.end:
            if self.p > self.patience:
                self.p = 0
                if self.idx + 1 >= len(self.prob_list):
                    self.end = True
                else:
                    self.idx += 1
                    
                if not self.end:
                
                    if self.mode == "min":
                        self.checkpoint = np.math.inf
                    elif self.mode == "max":
                        self.checkpoint = -np.math.inf
                    
                    if self.reset_lr:
                        for i, param_group in enumerate(self.optim.param_groups):
                            param_group["lr"] = self.lr
                    
                    self.probs = self.prob_list[self.idx]
        
        
    def active_probs(self):
        return self.probs
    
    def phase_best(self):
        return self.best[self.idx]
    
    def has_ended(self):
        return self.end

class Model(torch.nn.Module):
    def __init__(self, layers, hiddensize, outputsize, num_words, imagesize, vision = "vgg16", vision_layer = 30, masksize = 0, fc_output_size = 12, joints = 6, device=None):
        super(Model, self).__init__()
        #self.mask_labels = masking
        self.add_module("vision", features.VisionModule(model=vision, layer=vision_layer, imagesize=imagesize, device=device))
        self.vision.requires_grad_(False) # We are not training the feature extractor
        #self.vision = features.VisionModule(model=vision, layer=vision_layer, imagesize=imagesize, device=device).requires_grad_(False)
        self.add_module("dropout", torch.nn.Dropout(p = 0.5).to(device))
        #self.dropout = torch.nn.Dropout(p = 0.5)
        fc_input_size = self.vision(torch.zeros(imagesize, device=device).unsqueeze(0)).numel()
        self.add_module("fc0", torch.nn.Linear(fc_input_size, fc_output_size).to(device))
        #self.add_module("fc1", torch.nn.Linear(256, fc_output_size).to(device))
        #self.fc = torch.nn.Linear(fc_input_size, fc_output_size).to(device)
        #self.lstm = StackedLSTM(mLSTM, layers, fc_output_size + joints + num_words + masksize, hiddensize, outputsize, 0).to(device).to(device)
        #self.add_module("lstm", mLSTM(data_size = masksize + fc_output_size + joints + num_words, hidden_size = hiddensize).to(device))
        joints = 0
        self.add_module("lstm", torch.nn.LSTM(input_size = masksize + fc_output_size + joints + num_words, hidden_size = hiddensize, num_layers=layers).to(device))
        #self.lstm = torch.nn.LSTM(input_size = masksize + fc_output_size + joints + num_words, hidden_size = hiddensize, num_layers=layers).to(device)
        #self.h2o = torch.nn.Linear(hiddensize, outputsize).to(device)
        self.add_module("h2o", torch.nn.Linear(hiddensize, outputsize).to(device))
        #self.log_softmax = torch.nn.LogSoftmax(dim=2).to(device)
        
        
        self.num_layers = layers
        self.hidden_size = hiddensize

    def forward(self, x, h, m = []):
        vision = self.vision(x[0])
        vision = vision.flatten(start_dim=1)
        vision_fc_0 = self.fc0(vision)
       # vision_fc_1 = self.fc1(vision_fc_0)
        vision_fc_d = self.dropout(vision_fc_0)
        #m = torch.tensor(m, dtype=torch.float32, device = vision_fc.device)
        #m = m.to(dtype=torch.float32)
        lstm_input = torch.cat((m, vision_fc_d, x[2]), dim=1).unsqueeze(1)
        #lstm_input = torch.cat((m, vision_fc_d, x[1], x[2]), dim=1).unsqueeze(1)
        output, hidden = self.lstm(lstm_input, h)
        return hidden, self.h2o(output)
        #return self.prediction(torch.cat((vision.flatten(), x[1]), dim=0), h)
        
    def state0(self, batchsize):
        h_0 = torch.zeros(self.num_layers, batchsize, self.hidden_size, requires_grad=False)
        c_0 = torch.zeros(self.num_layers, batchsize, self.hidden_size, requires_grad=False)
        return (h_0, c_0)

class BatchModel(torch.nn.Module):
    def __init__(self, layers, hiddensize, outputsize, num_words, imagesize, vision = "vgg16", vision_layer = 30, masksize = 0, fc_output_size = 12, joints = 6, device=None):
        super(BatchModel, self).__init__()
        self.add_module("vision", features.VisionModule(model=vision, layer=vision_layer, imagesize=imagesize, device=device))
        self.vision.requires_grad_(False) # We are not training the feature extractor
        #self.vision = features.VisionModule(model=vision, layer=vision_layer, imagesize=imagesize, device=device).requires_grad_(False)
        self.add_module("dropout", torch.nn.Dropout(p = 0.5))
        #self.dropout = torch.nn.Dropout(p = 0.5)
        fc_input_size = self.vision(torch.zeros(imagesize[1:], device=device).unsqueeze(0)).numel()
        self.add_module("fc", torch.nn.Linear(fc_input_size, fc_output_size).to(device))
        #self.add_module("lstm", torch.nn.LSTM(input_size = masksize + fc_output_size + joints + num_words, hidden_size = hiddensize, num_layers=layers).to(device))
        self.add_module("lstm", torch.nn.LSTMCell(input_size = masksize + fc_output_size + joints + num_words, hidden_size = hiddensize).to(device))
        self.add_module("h2o", torch.nn.Linear(hiddensize, outputsize).to(device))
        
        self.num_layers = layers
        self.hidden_size = hiddensize
        
    def forward(self, x, h, m):
        vision = self.vision(x[0])
        vision_fc = self.fc(vision.flatten(start_dim=1))
        vision_fc = self.dropout(vision_fc)
        #m = torch.tensor(m, dtype=torch.float32, device = vision_fc.device)
        #m = m.repeat(batches, 1)
        lstm_input = torch.cat((m, vision_fc, x[1], x[2]), dim=1)#.unsqueeze(0)
        hidden = self.lstm(lstm_input, h)
        #output, hidden = self.lstm(lstm_input, h)
        return hidden, self.h2o(hidden[0])
        #return self.prediction(torch.cat((vision.flatten(), x[1]), dim=0), h)
        
    def state0(self, batchsize):
        h_0 = torch.zeros(self.num_layers, batchsize, self.hidden_size, requires_grad=False)
        c_0 = torch.zeros(self.num_layers, batchsize, self.hidden_size, requires_grad=False)
        h_0 = torch.zeros(batchsize, self.hidden_size, requires_grad=False)
        c_0 = torch.zeros(batchsize, self.hidden_size, requires_grad=False)
        return (h_0, c_0)
    
class CTCModel(torch.nn.Module):
    def __init__(self, layers, hiddensize, outputsize, words, fc_output_size = 12, imagesize = (3, 128, 128), joints = 6, device=None):
        super(CTCModel, self).__init__()
        self.vision = features.VisionModule(model="vgg16", layer=30, imagesize=imagesize, device=device).requires_grad_(False)
        self.dropout = torch.nn.Dropout(p = 0.5)
        fc_input_size = self.vision(torch.zeros(imagesize, device=device).unsqueeze(0)).numel()
        self.fc = torch.nn.Linear(fc_input_size, fc_output_size).to(device)
        #self.lstm = StackedLSTM(mLSTM, layers, fc_output_size + joints + words, hiddensize, outputsize, 0).to(device)
        self.lstm = torch.nn.LSTM(input_size = fc_output_size + joints, hidden_size = hiddensize, num_layers=layers).to(device)
        self.h2o = torch.nn.Linear(hiddensize, outputsize).to(device)
        #self.log_softmax = torch.nn.LogSoftmax(dim=2).to(device)
        
        self.num_layers = layers
        self.hidden_size = hiddensize

    def forward(self, x, h):
        vision = self.vision(x[0].unsqueeze(0))
        vision_fc = self.fc(vision.flatten())
        vision_fc = self.dropout(vision_fc)
        output, hidden = self.lstm(torch.cat((vision_fc, x[1]), dim=0).unsqueeze(0).unsqueeze(0), h)
        return hidden, self.h2o(output)
        #return self.prediction(torch.cat((vision.flatten(), x[1]), dim=0), h)
        
    def state0(self, batchsize):
        h_0 = torch.zeros(self.num_layers, batchsize, self.hidden_size, requires_grad=False)
        c_0 = torch.zeros(self.num_layers, batchsize, self.hidden_size, requires_grad=False)
        return (h_0, c_0)

def copy_state(state):
    if isinstance(state, tuple):
        #return (Variable(state[0].data), Variable(state[1].data))
        return (state[0].clone().detach().requires_grad_(True), state[1].clone().detach().requires_grad_(True))
    else:
        #return Variable(state.data)
        return state.clone().detach().requires_grad_(True)
    
def train(model, data, criterion, optimizer, loss, masks, mask_probs, masking, 
          k = 2, batchsize = 100, seq_length = 100, shuffle=False, device=None,
          n_workers = 0):
    if loss == "ctc":
        return train_ctc(model, data, criterion, optimizer, masks, k, batchsize, shuffle, device)
    elif loss == "crossentropy":
        return train_crossentropy(model, data, criterion, optimizer, masks, mask_probs, masking, k, batchsize, seq_length, shuffle, device,
                                  num_workers=n_workers)

def train_crossentropy(model, data, criterion, optimizer, mask_configs, mask_probs, masking, k, batchsize, seq_length, shuffle, device,
                       num_workers):
    if shuffle:
        data.shuffle()
    dl = DataLoader(data, batch_size=batchsize, shuffle=False, pin_memory = True, num_workers=num_workers)
    data.gen_masks(mask_configs, mask_probs, masking)
    
    model.train()
    hidden_init = model.state0(1)
    hidden = (hidden_init[0].to(device), hidden_init[1].to(device))
    
    running_loss = 0

    predictions = np.ndarray((len(data), k), dtype=int)
    ground_truth = np.ndarray((len(data),), dtype=int)
    c = 0
    seq_c = 0
    
#    use_masks = len(mask_configs) > 1
#    if use_masks:
        #active_mask = mask_configs[np.random.randint(len(mask_configs))]
#        active_mask = mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)]
        #sen = []
#    else:
#        active_mask = mask_configs[0]
    #active = np.random.randint(0, m)
    #mask_neurons = [1.0 if i == active else 0.0 for i in range(m)]

    loss = 0
    optimizer.zero_grad()
    for (batch_img, batch_joints, batch_lang, batch_y, batch_mask, _) in tqdm(dl, desc="Training"):
        #hidden = hidden_init
        #optimizer.zero_grad()
        X_img = batch_img.to(device, non_blocking = True)
        X_joint = batch_joints.to(device, non_blocking = True)
        X_lang = batch_lang.to(device, non_blocking = True)
        y = batch_y.to(device, non_blocking = True)
        mask = batch_mask.to(device, non_blocking = False)
        #end = batch_end.to(device, non_blocking = False)
        
        #loss = 0
#        for t in range(1):# range(min(batchsize, len(batch_img))):
#            if use_masks:
#                l_pos = X_lang[t].argmax()
#                if l_pos > 0 and active_mask[masking[l_pos] - 1] == 0:
#                    X_lang[t][l_pos] = 0
#                    X_lang[t][0] = 1
                    
#            if seq_c == 0:
#        optimizer.zero_grad()
#        loss = 0
            
        hidden, output = model((X_img, X_joint, X_lang), hidden, mask)#, active_mask)
        del X_img, X_joint, X_lang
        output = output.squeeze(1)
            #loss += criterion(output, y[t].topk(1)[1])

        predictions[c : c + len(batch_y)] = output.cpu().topk(k)[1].squeeze()
        label = y.topk(1)[1].squeeze()
#            if use_masks:
#                if label.item() > 0 and active_mask[masking[label.item()] - 1] == 0:
#                    label[0] = 0
                #sen.append(label.item())
#                if end[t]:
                    #active_mask = mask_configs[np.random.randint(len(mask_configs))]
#                   if use_masks:
        """
        if len(np.unique(sen)) > 2:
            print("\nMask:", active_mask)
            print("Sentence:", sen)
            print(data.index_map[c])
        sen = []
        """
#                        active_mask = mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)]
                    #m = np.random.randint(low = 1, high=mask_max)
                    #active = np.random.randint(0, m)
                    #mask_neurons = [1.0 if i == active else 0.0 for i in range(m)]
                    #if np.count_nonzero(mask_neurons) == 0:
                    #    mask_neurons[np.random.randint(0, m)] = 1.0
                    #mask_neurons = [float(np.random.randint(0, 2)) for i in range(m)]

        ground_truth[c : c+ len(batch_y)] = label.cpu().squeeze()
            
        c += len(batch_y)
            
        loss += criterion(output, label)
        seq_c += 1
        if seq_c >= seq_length:
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            hidden = copy_state(hidden)
            loss = 0
            seq_c = 0
            optimizer.zero_grad()
        del y, mask
#        seq_c += len(batch_y)
#        if seq_c == seq_length:
#        loss.backward()
#        optimizer.step()
#            seq_c = 0
#        running_loss += loss.item()
#        hidden = copy_state(hidden)
                
            
        #loss.backward()
        #optimizer.step()

        #running_loss += loss.item()
        
        #hidden_init = copy_state(hidden)

    loss_avg = running_loss / len(data)

    return loss_avg, predictions, ground_truth


def batch_train_crossentropy(model, data, criterion, optimizer, mask_configs, mask_probs, masking, k, batchsize, seq_length, shuffle, device, num_workers):
    if shuffle:
        data.shuffle()
    dl = DataLoader(data, batch_size=batchsize, shuffle=False, pin_memory = True, num_workers=num_workers)
    
    model.train()
    hidden_init = model.state0(data.batches)
    hidden = (hidden_init[0].to(device), hidden_init[1].to(device))
    
    running_loss = 0

    predictions = np.ndarray((data.frames, k), dtype=int)
    ground_truth = np.ndarray((data.frames,), dtype=int)
    c = 0
    seq_c = 0
    
    use_masks = len(mask_configs) > 1
    if use_masks:
        #active_mask = mask_configs[np.random.randint(len(mask_configs))]
        active_masks = [mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)] for i in range(data.batches)]
        #sen = []
    else:
        active_masks = [mask_configs[0]] * data.batches
    active_masks = torch.tensor(active_masks, dtype=torch.float32, device=device)
    #active = np.random.randint(0, m)
    #mask_neurons = [1.0 if i == active else 0.0 for i in range(m)]

    for (batch_img, batch_joints, batch_lang, batch_y, batch_end) in tqdm(dl, desc="Training"):
        #hidden = hidden_init
        #optimizer.zero_grad()
        X_img = batch_img.to(device, non_blocking = True)
        X_joint = batch_joints.to(device, non_blocking = True)
        X_lang = batch_lang.to(device, non_blocking = True)
        y = batch_y.to(device, non_blocking = True)
        end = batch_end.to(device, non_blocking = False)
        
        #loss = 0
        for t in range(min(batchsize, len(batch_img))):
            if use_masks:
                for i in range(data.batches):
                    l_pos = X_lang[t, i].argmax()
                    if l_pos > 0 and active_masks[i][masking[l_pos] - 1] == 0:
                        X_lang[t, i, l_pos] = 0.0
                        X_lang[t, i, 0] = 1.0
                    
            if seq_c == 0:
                optimizer.zero_grad()
                loss = 0
            hidden, output = model((X_img[t], X_joint[t], X_lang[t]), hidden, active_masks)
            output = output.squeeze(0)
            #loss += criterion(output, y[t].topk(1)[1])
            predictions[(data.batch_limits[:-1] + c) % data.batch_limits[-1]] = output.squeeze().topk(k)[1].cpu().data
            label = y[t].topk(1)[1].squeeze(1)
            if use_masks:
                for i in range(data.batches):
                    if label[i].item() > 0 and active_masks[i][masking[label[i].item()] - 1] == 0:
                        label[i] = 0
                    #sen.append(label.item())
                    if end[t, i]:
                        #active_mask = mask_configs[np.random.randint(len(mask_configs))]
                        active_masks[i] = torch.tensor(mask_configs[np.random.choice(np.arange(len(mask_configs)), p = mask_probs)])

            for i in range(data.batches):
                ground_truth[(c + data.batch_limits[i]) % data.batch_limits[-1]] = label[i].item()
                
            print(ground_truth)
            c += 1
            loss += criterion(output, label)
            seq_c += 1
            if seq_c == seq_length:
                loss.backward()
                optimizer.step()
                seq_c = 0
                running_loss += loss.item()
                hidden = copy_state(hidden)
                
            
    if seq_c > 0:
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        #loss.backward()
        #optimizer.step()

        #running_loss += loss.item()
        
        #hidden_init = copy_state(hidden)

    loss_avg = running_loss / c

    return loss_avg, predictions, ground_truth

def train_ctc(model, data, criterion, optimizer, k, batchsize, shuffle, masking, device):
    #data = datasets.SimpleDataset(directory)
    if shuffle:
        data.shuffle()
    dl = DataLoader(data, batch_size=batchsize, shuffle=False, num_workers=8)
    
    model.train()
    #hidden_init = model.lstm.state0(1)
    hidden_init = model.state0(1)
    hidden_init = (hidden_init[0].to(device), hidden_init[1].to(device))
    
    running_loss = 0

    predictions = np.ndarray((len(data), k), dtype=int)
    ground_truth = np.ndarray((len(data),), dtype=int)
    c = 0
    
    ctc_inputs = []
    ctc_targets = []
    hidden = hidden_init
    
    optimizer.zero_grad()
    
    for (batch_img, batch_joints, batch_lang, batch_y) in tqdm(dl, desc="Training"):
        #hidden = hidden_init
        #optimizer.zero_grad()
        X_img = batch_img.to(device)
        X_joint = batch_joints.to(device)
        #X_lang = batch_lang.to(device)
        y = batch_y.to(device)
        torch.autograd.set_detect_anomaly(True)
        loss = 0
        for t in range(min(batchsize, len(batch_img))):
            hidden, output = model((X_img[t], X_joint[t]), hidden)
            #output = output.squeeze(0)
            ctc_inputs.append(output)
            ctc_targets.append(y[t].topk(1)[1])
            
            #loss += criterion(output, y[t].topk(1)[1])

            #predictions[c] = pred
            predictions[c] = output.squeeze().topk(k)[1].cpu().data
            ground_truth[c] = y[t].argmax()
            c += 1
            if c in data.action_perimeters or c == len(data):
            #if not batch_mid_action[t]:
                #optimizer.zero_grad()
                #print("input size:", torch.cat(ctc_inputs).unsqueeze(1).shape)
                input = torch.cat(ctc_inputs).log_softmax(2)
                
                target = torch.tensor(ctc_targets, dtype=torch.float)
                target = target[target.nonzero().transpose(0,1)[0]] # unique_consecutive().unsqueeze(0)
                target = target.unique_consecutive()
                #target = torch.full(target.shape, 1)
                #print(target.shape)
                #target = torch.tensor([1, 2], dtype=float)
                #target = target.unique_consecutive()
                #print("target size:", t.shape)
                #print("input length:", torch.tensor((len(ctc_inputs))))
                #print("target length:", torch.tensor((len(ctc_inputs))))
                
                #print("\n")
                #print(torch.tensor(ctc_targets))
                
                #print("\n")
                #print(t)
                #print("input size:", input.shape)
                #print("input:", input)
                #print("BOOL:", input)
                #print("target length:", len(t))
                #print("target:", target)
                loss = criterion(input, target, (len(input),), (len(target),))
                loss.backward()
                #print("Loss:", loss)
                optimizer.step()
                running_loss += loss.item()
                optimizer.zero_grad()
                ctc_inputs.clear()
                ctc_targets.clear()
                hidden = copy_state(hidden)
        
        #hidden = hidden_init
    """
    input = torch.cat(ctc_inputs)
    target = torch.tensor(ctc_targets)
    target = target[target.nonzero().transpose(0,1)[0]]# .unique_consecutive().unsqueeze(0)
    target = target.unique_consecutive()
    loss = criterion(input, target, (len(input),), (len(target),))
    loss.backward()
    optimizer.step()
    
    running_loss += loss.item()
    """ 
    #hidden_init = copy_state(hidden)


    loss_avg = running_loss / data.action_count
    """
    top1_accuracy = top1_hits / len(data)
    topk_accuracy = topk_hits / len(data)
    top1_nonZero_accuracy = top1_nonZero_hits / nonZero_samples
    topk_nonZero_accuracy = topk_nonZero_hits / nonZero_samples
    sentence_accuracy = sentence_hits / (sentence_hits + sentence_misses)
    #print("sentences: ", sentence_hits + sentence_misses, sentence_hits, sentence_misses)
    #print("in data: ", data.action_count)

    print("top1:", top1_accuracy, " | ", metrics[0])
    print("topk:", topk_accuracy, " | ", metrics[1])
    print("top1_n0:", top1_nonZero_accuracy, " | ", metrics[2])
    print("topk_n0:", topk_nonZero_accuracy, " | ", metrics[3])
    print("sentence:", sentence_accuracy, " | ", metrics[4])
    print("confusion_matrix:\n", metrics[5])
    print("sentence_pairs:\n", metrics[6])
    """
    return loss_avg, predictions, ground_truth
    #return loss_avg, top1_accuracy, topk_accuracy, top1_nonZero_accuracy, topk_nonZero_accuracy, sentence_accuracy

#def validate(model, data, criterion, k = 2, batchsize = 100):
 #   return test.test(model, data, criterion, k = k, batchsize= batchsize, cuda=cuda)

def printlog(level, *args):
    global verbose
    if level >= verbose:
        print(*args)
        
verbose = 1


def save_state(naming, model, var, mask_scheduler, learning_rate_scheduler):
    model_save_path = os.path.join(weights_path, "net_" + naming + ".pt")
    torch.save(model.state_dict(), model_save_path)
    pickle.dump([model_save_path, var, mask_scheduler, learning_rate_scheduler] , open(os.path.join(weights_path, "state_" + naming + ".p"), "wb"))
    #training_variables.e = epoch
    #with open(os.path.join(weights_path, "state_" + naming + ".txt"), "w") as file:
    #    json.dump(training_variables.__dict__, file, indent = 2)
            
def load_state(path, model, device):
    p = pickle.load(open(path, "rb"))
    model_path = p[0]
    training_variables = p[1]
    mask_scheduler = p[2]
    learning_rate_scheduler = p[3]
    model.load_state_dict(torch.load(model_path, map_location=torch.device(device)))
    return training_variables, mask_scheduler, learning_rate_scheduler


def save_training_info(file_path, e, add_lines = []):
    global START_DATETIME
    with open(file_path, "w") as file:
        file.write("Training started at " + str(START_DATETIME))
        file.write("Trained for " + str(e) + " epochs")
        file.write("At time " + str(datetime.datetime.today()))
        """
        if args.early_stopping and p < 0:
            file.write("Training stopped early after " + str(e) + " epochs")
        else:
            file.write("Training ended after " + str(e) + " epochs")
        """
        file.write("\n")
        file.write("\n".join(add_lines))

#def validate(model, data, loss, k = 2, batchsize = 10):
#    return test.test(model, data, loss, k, batchsize)
"""
def validate_preload(model, data, loss, k = 2):
    return test.test_preload(model, data, loss, k)
"""      
if __name__ == "__main__":
    
    import argparse
    argparser = argparse.ArgumentParser(description="train")
    argparser.add_argument("data", help="Path to data set file")
    argparser.add_argument("backup", help="Backup folder for model weights")
    argparser.add_argument("-ignore_not_empty", action="store_true", help="Don't stop the program if the backup directory is not empty")
    argparser.add_argument("-resume", "--resume_training", default=None, help="Path to training state file")
    argparser.add_argument("-valid", help="Data used for validation")
    argparser.add_argument("-test", "--tests", help="Test file")
    #argparser.add_argument("-test1", default=None, help="Data used for first final test")
    #argparser.add_argument("-test2", default=None, help="Data used for second final test")
    argparser.add_argument("-words", default="", help="Labels")
    argparser.add_argument("-lr", "--learning_rate", type=float, default = 1e-3, help="Initial learning rate")
    argparser.add_argument("-lr_s", "--lr_scheduler", action="store_true", help="Use the learning rate scheduler")
    argparser.add_argument("-hidden", "--hidden_size", type=int, default = 256, help="Hidden layer size of lstm")
    argparser.add_argument("-layers", "--hidden_layers", type=int, default = 1, help="Number of hidden layer")
    argparser.add_argument("-dense", "--dense_size", type=int, default = 32, help="Size of fully-connected layer from vision output to the lstm")
    argparser.add_argument("-v", "--vision_model", default = "vgg16", help="Which feature extractor to use")
    argparser.add_argument("-vl", "--vision_layer", type=int, help="Which layer of the feature extractor model to use")
    argparser.add_argument("-mask", help="Path to file with masking info")
    argparser.add_argument("-k", default = 2, type=int, help="top k")
    argparser.add_argument("-epochs", default = 500, type=int, help="Number of training epochs")
    argparser.add_argument("-early", "--early_stopping", action="store_true", help="Train with early stopping")
    argparser.add_argument("-p", "--patience", default = 5, type=int, help="Patience for early stopping")
    argparser.add_argument("-workers", "--num_workers", default = 4, type=int, help="Number of parallel workers for data loading")
    argparser.add_argument("-n_batches", default = 125, type=int, help="Number of batches")
    argparser.add_argument("-batch", "--batch_size", default = 1, type=int, help="Batch size")
    argparser.add_argument("-seq", "--seq_length", default=2, type=int, help="After how many frames the loss is backpropagated")
    argparser.add_argument("-shuffle", action="store_true")
    argparser.add_argument("-cuda", dest="use_cuda", action="store_true")
    argparser.add_argument("-gpu", default = 0, type=int, help="Select which gpu is used for training")
    argparser.add_argument("-loss", default = "crossentropy", help="Options: crossentropy, ctc")
    argparser.add_argument("-i", "--eval_intervall", default = 1, type=int, help="Evaluates the model in the given intervalls on the validation set")
    argparser.add_argument("-m", "--mask_intervall", default = 5, type=int, help="Define how often the model should be evaluated on all possible masking options")
    argparser.add_argument("-s", "--save_intervall", default = 5, type=int, help="Save backups at the specified intervall")
    argparser.add_argument("-simple", dest="simple", action="store_true", help="Train on the simple data")
    argparser.add_argument("-preload", action="store_true", help="Preload the euse_cudantire datasets into the memory")
    argparser.add_argument("-verbose", default = 1)
    argparser.set_defaults(vision_layer=None, valid="", mask="", lr_scheduler=False, early_stopping = False, shuffle=False, use_cuda=False, simple=False, preload=False, ignore_not_empty=False)

    args = argparser.parse_args()

    if os.listdir(args.backup):
        if not args.ignore_not_empty:
            sys.exit("BACKUP DIR NOT EMPTY - KILLED")
        else:
            print("BACKUP DIR NOT EMPTY")
    if args.tests is not None:
        with open(args.tests, "r") as file:
            test_paths = file.read().strip().splitlines()
            f = torchvision.transforms.ToTensor()
            for i, line in enumerate(test_paths):
                print("Test set 1 " + str(i) + ":", line)
                check_data_set = datasets.ActionDataset(line, args.words, transf = f)
        del check_data_set
        del file
    
    if args.resume_training is not None:
        params = argparse.Namespace()
        with open(os.path.join(args.backup, "commandline_args.txt"), "r") as f:
            params.__dict__ = json.load(f)
            params.resume_training = args.resume_training
            params.use_cuda = args.use_cuda
            params.gpu = args.gpu
            params.num_workers = args.num_workers
            
        args = params
            
    train_path = args.data
    valid_path = args.valid
    if valid_path == "": valid_path = train_path
    #weights_path = args.backup + str(datetime.datetime.today())
    if args.resume_training is None:
        weights_path = os.path.join(args.backup, str(datetime.datetime.today()))
    else:
        weights_path = os.path.dirname(args.resume_training)
    
    writer = SummaryWriter(log_dir = weights_path)
    
    csv_path = os.path.join(weights_path, "predictions")
    if not os.path.exists(csv_path):
        os.mkdir(csv_path)
    
    from shutil import copy
    copy(__file__, os.path.join(weights_path, os.path.basename(__file__)))
    with open(os.path.join(weights_path, "params.txt"), "w") as file:
        file.write("hidden layer size: " + str(args.hidden_size))
        file.write("\nhidden layers: " + str(args.hidden_layers))
        file.write("\ndense layer size: " + str(args.dense_size))
        file.write("\nfeature extraction model: " + args.vision_model)
        file.write("\nfeature extraction layer: " + str(args.vision_layer))
        
    with open(os.path.join(weights_path, "commandline_args.txt"), "w") as file:
        json.dump(args.__dict__, file, indent = 2)
    
    PYTORCH_DEVICE = torch.device("cpu")
    if args.use_cuda:
        printlog(1, "Using CUDA...")
        PYTORCH_DEVICE = torch.device("cuda:"+ str(args.gpu))
        torch.cuda.set_device(args.gpu)
        #torch.set_default_tensor_type(torch.cuda.DoubleTensor)
        printlog(1, "Using device", "cuda:"+str(args.gpu))
    else:
        printlog(1, "Using CPU...")
        #torch.set_default_tensor_type(torch.FloatTensor)
        
    if args.mask != "":
        masking = np.loadtxt(args.mask, dtype=int)
        masksize = np.count_nonzero(np.unique(masking))
        import itertools
        masks = torch.tensor(list(itertools.product([0, 1], repeat=masksize)), requires_grad=False, dtype=torch.float32)
        #mask_probabilities = np.array([[1/7, 2/7, 2/7, 0, 2/7, 0, 0, 0], 
        #                               [1/13, 2/13, 2/13, 2/13, 2/13, 2/13, 2/13, 0],
        #                               [1/15, 2/15, 2/15, 2/15, 2/15, 2/15, 2/15, 2/15]])
        mask_probabilities = np.array([[0/20, 1/20, 1/20, 3/20, 1/20, 3/20, 3/20, 8/20]])
        #mask_probabilities = np.array([[1/15, 2/15, 2/15, 2/15, 2/15, 2/15, 2/15, 2/15]])
        default_mask = (1,) * masksize
        default_mask = torch.tensor(default_mask, dtype=torch.float32)
    else:
        masking = None
        masksize = 0
        masks = [()]
        mask_probabilities = np.array([[1]])
        default_mask = ()
        masks = torch.tensor([default_mask])
        default_mask = torch.tensor(default_mask, dtype=torch.float32)
        
    """
    if args.simple:
        train_data = datasets.SimpleDataset(train_path)
        if valid_path == train_path:
            valid_data = train_data
        else:
            valid_data = datasets.SimpleDataset(valid_path)

    else:
    """
        #f = torchvision.transforms.Compose([
        #                        torchvision.transforms.Resize(224),
        #                        torchvision.transforms.CenterCrop(224),
        #                        torchvision.transforms.ToTensor(),
        #                        torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
        
    N_WORKERS = args.num_workers
    NUM_BATCHES = args.n_batches
    BATCHSIZE = args.batch_size
    
    if args.vision_model == "yolo":
        f = torchvision.transforms.Compose([torchvision.transforms.Resize((224, 224)),
                                            torchvision.transforms.ToTensor()])
    else:
        f = torchvision.transforms.ToTensor()
    train_data = datasets.ActionDataset(train_path, args.words, transf = f)
    if valid_path == train_path:
        valid_data = train_data
        printlog(1, "Using training data for validation")
    else:
        valid_data = datasets.ActionDataset(valid_path, args.words, transf = f)
    """
    train_data = datasets.ActionDataset(train_path, args.words, transf = f)
    if valid_path == train_path:
        valid_data = train_data
        printlog(1, "Using training data for validation")
    else:
        valid_data = datasets.ActionDataset(valid_path, args.words, transf = f)
    """
    """
        if args.preload:
            train_data = datasets.ImageDataset(train_path, transf=torchvision.transforms.ToTensor())
            if valid_path == train_path:
                valid_data = train_data
            else:
                valid_data = datasets.ImageDataset(valid_path, transf=torchvision.transforms.ToTensor())

        else:
            train_data = datasets.VisionDataset(train_path, device=device)
            if valid_path == train_path:
                valid_data = train_data
            else:
                valid_data = datasets.VisionDataset(valid_path, device=device)
    """         
    
    SEQUENCESIZE = args.seq_length
    LSTM_LAYERS = args.hidden_layers
    HIDDEN_SIZE = args.hidden_size
    DENSE_SIZE = args.dense_size
    OUTPUT_SIZE = train_data.word_count
    VISION_OUT_LAYER = args.vision_layer

    if VISION_OUT_LAYER == None: # set default output layers
        if args.vision_model == "vgg16":
            vision_layer = 30
        elif args.vision_model == "yolo":
            vision_layer = 64

    if args.loss == "crossentropy":
        model = Model(LSTM_LAYERS, HIDDEN_SIZE, OUTPUT_SIZE, train_data.word_count, imagesize=train_data[0][0].shape,
                      vision=args.vision_model, vision_layer=VISION_OUT_LAYER, masksize=masksize, fc_output_size = DENSE_SIZE, device=PYTORCH_DEVICE)
        
        #model = Model(LSTM_LAYERS, HIDDEN_SIZE, OUTPUT_SIZE, train_data.word_count, imagesize=train_data[0][0].shape,
        #              vision=args.vision_model, vision_layer=VISION_OUT_LAYER, masksize=masksize, fc_output_size = DENSE_SIZE, device=PYTORCH_DEVICE)
    elif args.loss == "ctc":
        model = CTCModel(LSTM_LAYERS, HIDDEN_SIZE, OUTPUT_SIZE, len(train_data.words), fc_output_size = DENSE_SIZE, imagesize=train_data[0][0].shape, device=PYTORCH_DEVICE)
    
    print("Image shape:", train_data[0][0].shape)

    #lstm = StackedLSTM(mLSTM, layers, input_size, hidden_size, output_size, 0)
    nParams_LSTM = sum([p.nelement() for p in model.lstm.parameters()] + [p.nelement() for p in model.h2o.parameters()])
    nParams_Linear = sum([p.nelement() for p in model.fc0.parameters()])
    print("LSTM parameters:", nParams_LSTM)
    print("Linear parameters:", nParams_Linear)
    #if args.use_cuda:
     #   model.to(device)

    if args.loss == "crossentropy":
        criterion = CrossEntropyLoss(reduction="sum")
    elif args.loss == "ctc":
        criterion = CTCLoss()

    

    
    if args.resume_training is not None:
        vargs, mask_scheduler, lr_scheduler = load_state(args.resume_training, model, PYTORCH_DEVICE)
        lr = vargs["lr"]
        e = vargs["epoch"]
        best_epoch = vargs["best_epoch"]
        best_validation_accuracy = vargs["best_val_accuracy"]
        
    else:
        lr = args.learning_rate
        e = 0
        best_epoch = 0
        best_validation_accuracy = 0
        
    #trainable_params = list(model.fc0.parameters()) + list(model.fc1.parameters()) + list(model.lstm.parameters()) + list(model.h2o.parameters())

    trainable_params = list(model.fc0.parameters()) + list(model.lstm.parameters()) + list(model.h2o.parameters())


    optimizer = Adam(trainable_params, lr = lr)
    
    if args.resume_training is None:
        lr_scheduler = ReduceLROnPlateau(optimizer, "max", patience=args.patience, verbose=True)
        mask_scheduler = MaskScheduler(args.patience + 2, mask_probabilities, mode="max", optimizer=optimizer, lr = args.learning_rate)
    else:
        lr_scheduler.optimizer = optimizer
        mask_scheduler.optim = optimizer
    
    #training_variables = argparse.Namespace()
    """
    if args.resume_training is not None:
        with open(args.resume_training, "r") as f:
            training_variables.__dict__ = json.load(f)
        
        model.load_state_dict(torch.load(args.resume_training.replace("state", "net").replace(".txt", ".pt"), map_location=torch.device(PYTORCH_DEVICE)))

    else:
        training_variables.best_validation_accuracy = 0
        training_variables.best_epoch = 0
        training_variables.p = args.patience
        training_variables.e = 0
        training_variables.lr = optimizer.param_groups[0]["lr"]
    """
    """
    if args.use_cuda:
        hidden_init = model.state0(1)
        writer.add_graph(model, ((train_data[0][0], train_data[0][1], train_data[0][2]), (hidden_init[0].to(PYTORCH_DEVICE), hidden_init[1].to(PYTORCH_DEVICE)), default_mask))
    else:
        img, j, l, _, _, _ = train_data[0]
        writer.add_graph(model, input_to_model = ((img.unsqueeze(0), j.unsqueeze(0), l.unsqueeze(0)), model.state0(1), default_mask))
    """
    START_DATETIME = datetime.datetime.today()
    start = time.time()
    for e in range(e + 1, args.epochs + 1): # Start with epoch 1 instead of 0
        #print("BS:", batch_size)
        print("Epoch:", e)
        print("Learning rate:", optimizer.param_groups[0]["lr"])
        print("Using masks and probabilities:")
        for i, elem in enumerate(masks):
            print(elem, "-", mask_scheduler.active_probs()[i])
        tr_loss, predictions, ground_truth = train(model, train_data, criterion, optimizer, masks = masks, mask_probs=mask_scheduler.active_probs(), 
                                                   masking = masking, loss=args.loss, k=args.k, batchsize= NUM_BATCHES, seq_length=SEQUENCESIZE,
                                                   shuffle=args.shuffle, device=PYTORCH_DEVICE, n_workers=N_WORKERS)
        tr_accuracy, tr_k_accuracy, tr_n0_accuracy, tr_k_n0_accuracy, tr_sen_accuracy, tr_sen_n0_accuracy, _, _, tr_accs = test.evaluation(predictions, ground_truth, train_data.action_perimeters, labels=train_data.words, sentence_tracking=True, save_file=os.path.join(csv_path, "train_" + str(e)))
        #alt_accuracy = test.test_sentences(lstm, train_data, batch_size = batch_size)
        #print("Alt. training acc:", alt_accuracy)
        print(":\nTraining acc:", tr_accuracy,
              "\nTopk acc:", tr_k_accuracy,
              "\nSentence-wise acc:", tr_sen_accuracy,
              "\nSentence-wise acc (ignore blanks):", tr_sen_n0_accuracy,
              "\nTraining n0 acc:", tr_n0_accuracy, "\nn0 topk acc:", tr_k_n0_accuracy,
              "\nTraining loss:", tr_loss)
        
        writer.add_scalar("Learning rate", optimizer.param_groups[0]["lr"], e)
        writer.add_scalar("Loss/train", tr_loss, e)
        writer.add_scalar("Top-1-accuracy/train", tr_accuracy, e)
        writer.add_scalar("Top-"+str(args.k)+"-accuracy/train", tr_k_accuracy, e)
        writer.add_scalar("Sentence-wise accuracy/train", tr_sen_accuracy, e)
        writer.add_scalar("Sentence-wise accuracy (ignore blanks)/train", tr_sen_n0_accuracy, e)
        writer.add_scalar("Top-1-non0-accuracy/train", tr_n0_accuracy, e)
        writer.add_scalar("Top-"+str(args.k)+"-n0-accuracy/train", tr_k_n0_accuracy, e)
        """
        w = {t: sum(df[(df["Ground Truth"] == t) & (df["Correct"] == True)]["Occurrences"]) / 
                   sum(df[df["Ground Truth"] == t]["Occurrences"]) 
                   for t in df["Ground Truth"]}
        writer.add_scalars("Detailed Sentence-wise accuracy/train", w, e)
        """
        #writer.add_scalar("Sentence accuracy/train", alt_accuracy, e)
        if e % args.eval_intervall == 0:
            val_loss, predictions, ground_truth = test.test(model=model, data=valid_data, criterion=criterion, 
                                                                mask_configs=masks, mask_probs=mask_scheduler.active_probs(), 
                                                                masking = masking, loss = args.loss, k=args.k,
                                                                batchsize = NUM_BATCHES, seq_length= SEQUENCESIZE,
                                                                shuffle = args.shuffle, device = PYTORCH_DEVICE, n_workers=N_WORKERS)
            val_accuracy, val_k_accuracy, val_n0_accuracy, val_k_n0_accuracy, val_sen_accuracy, val_sen_n0_accuracy, _, _, ts_accs = test.evaluation(predictions, ground_truth,
                                                                                                 valid_data.action_perimeters, labels=valid_data.words, 
                                                                                                 sentence_tracking=True, save_file=os.path.join(csv_path, "valid_" + str(e)))
            """
            if masksize > 0:
                _, _, _, _, mask_sen_accuracy, mask_sen_n0_accuracy, _, _ = test.evaluation(mask_predictions, mask_ground_truth, valid_data.action_perimeters,
                                                                                                labels=valid_data.words, sentence_tracking=True, save_file=os.path.join(weights_path, "masked_valid.csv"))
                writer.add_scalar("Masked Sentence-wise accuracy/valid", mask_sen_accuracy, e)
                writer.add_scalar("Masked Sentence-wise accuracy (ignore blanks)/valid", mask_sen_n0_accuracy, e)
            """ 
            """
            if (e - 1) % args.mask_intervall == 0:
                val_loss, predictions, ground_truth, mask_predictions = test.test(model=model, data=valid_data, criterion=criterion, masks = masks, loss=args.loss, k=args.k, shuffle=args.shuffle, batchsize=batch_size, device=device)
                val_accuracy, val_k_accuracy, val_n0_accuracy, val_k_n0_accuracy, val_sen_accuracy, val_sen_n0_accuracy, _, _ = test.evaluation(predictions, ground_truth, valid_data.action_perimeters, labels=valid_data.words, mask_neurons=default_mask, masking=masking, sentence_tracking=True, save_file=os.path.join(weights_path, "valid.csv"))
                for i, m in enumerate(masks):
                    _, _, _, _, mask_sen_accuracy, mask_sen_n0_accuracy, _, _ = test.evaluation(mask_predictions[i], ground_truth, valid_data.action_perimeters, labels=valid_data.words, mask_neurons=m, masking=masking, sentence_tracking=True, save_file=os.path.join(weights_path, "".join([str(x) for x in m])+"_valid.csv"))
                    writer.add_scalar(str(m) + "/valid", mask_sen_accuracy, e)
                    writer.add_scalar(str(m) + "_n0/valid", mask_sen_n0_accuracy, e)
            else:
                val_loss, predictions, ground_truth, mask_predictions = test.test(model=model, data=valid_data, criterion=criterion, masks = [default_mask], loss=args.loss, k=args.k, batchsize=batch_size, device=device)
                val_accuracy, val_k_accuracy, val_n0_accuracy, val_k_n0_accuracy, val_sen_accuracy, val_sen_n0_accuracy, _, _ = test.evaluation(predictions, ground_truth, valid_data.action_perimeters, labels=valid_data.words, mask_neurons=default_mask, masking=masking, sentence_tracking=True, save_file=os.path.join(weights_path, "valid.csv"))
            """    
            #alt_accuracy = test.test_sentences(lstm, valid_data, batch_size = batch_size)
            #print("Alt. validation acc:", alt_accuracy)
            print("\tValidation acc:", val_accuracy, "\nTopk acc:", val_k_accuracy,
                  "\nSentence-wise acc:", val_sen_accuracy,
                  "\nSentence-wise acc (ignore blanks):", val_sen_n0_accuracy,
                  "\nValidation n0 acc:", val_n0_accuracy, "\nn0 topk acc:", val_k_n0_accuracy,
                  "\nValidation loss:", val_loss)
            writer.add_scalar("Loss/valid", val_loss, e)
            writer.add_scalar("Top-1-accuracy/valid", val_accuracy, e)
            writer.add_scalar("Top-"+str(args.k)+"-accuracy/valid", val_k_accuracy, e)
            writer.add_scalar("Sentence-wise accuracy/valid", val_sen_accuracy, e)
            writer.add_scalar("Sentence-wise accuracy (ignore blanks)/valid", val_sen_n0_accuracy, e)
            writer.add_scalar("Top-1-non0-accuracy/valid", val_n0_accuracy, e)
            writer.add_scalar("Top-"+str(args.k)+"-n0-accuracy/valid", val_k_n0_accuracy, e)
            writer.add_scalars
            
            #tracked_sentences = ["pushed left yellow banana"]
            for elem in sorted(tr_accs.keys()):
                writer.add_scalars("accuracy by sentence", {elem + "/train": tr_accs[elem][0]}, e)
                writer.add_scalars("support by sentence", {elem + "/train": tr_accs[elem][1]}, e)
                
            for elem in sorted(ts_accs.keys()):
                writer.add_scalars("accuracy by sentence", {elem + "/valid": ts_accs[elem][0]}, e)
                writer.add_scalars("support by sentence", {elem + "/valid": ts_accs[elem][1]}, e)
            """
            for elem in sorted(set(list(ts_accs.keys()) + list(tr_accs.keys()))):
            #for ts in tracked_sentences:
                    writer.add_scalars("accuracy by sentence", {elem + "/train": tr_accs[elem][0],
                                                            elem + "/valid": ts_accs[elem][0]}, e)
                    writer.add_scalars("support by sentence", {elem + "/train": tr_accs[elem][1],
                                                           elem + "/valid": ts_accs[elem][1]}, e)
                except KeyError:
                    pass
            """
            """
            w = {t: sum(df[(df["Ground Truth"] == t) & (df["Correct"] == True)]["Occurrences"]) / 
                       sum(df[df["Ground Truth"] == t]["Occurrences"]) 
                       for t in df["Ground Truth"]}
                
            writer.add_scalars("Detailed Sentence-wise accuracy/valid", w, e)
            """
            if args.lr_scheduler:
                lr_scheduler.step(val_sen_accuracy)
            #mask_scheduler.step(val_sen_n0_accuracy)
            mask_scheduler.step(val_sen_accuracy)
            #writer.add_scalar("Sentence accuracy/valid", alt_accuracy, e)
            if mask_scheduler.phase_best() != best_validation_accuracy:
                best_validation_accuracy = mask_scheduler.phase_best()
                best_epoch = e
                #torch.save(model.state_dict(), os.path.join(weights_path, "net_best.pt"))
                save_state("best", model, {"lr": optimizer.param_groups[0]["lr"], "epoch": e, "best_epoch": best_epoch, "best_val_accuracy": best_validation_accuracy},
                           mask_scheduler, lr_scheduler)
                print("Saved new best model")
                
            save_state("last", model, {"lr": optimizer.param_groups[0]["lr"], "epoch": e, "best_epoch": best_epoch, "best_val_accuracy": best_validation_accuracy},
                       mask_scheduler, lr_scheduler)
            save_training_info(os.path.join(weights_path, "train.info"), e, 
                               add_lines = ["Best validation accuracy: " + str(best_validation_accuracy),
                                            "Best epoch: " + str(best_epoch),
                                            str(train_data.words),
                                            "Output shape: ", str(OUTPUT_SIZE),
                                            "# lstm parameters: ", str(nParams_LSTM),
                                            "# linear parameters: ", str(nParams_Linear)])
                
            if mask_scheduler.has_ended() and args.early_stopping:
                break;
            """
            if val_sen_n0_accuracy > best_validation_accuracy:
                best_validation_accuracy = val_n0_accuracy
                best_epoch = e
                torch.save(model.state_dict(), os.path.join(weights_path, "net_best.pt"))
                print("Saved new best model")
            elif args.early_stopping:
                p -= 1
                if p < 0:
                    break;
            """
                    
        if e % args.save_intervall == 0:
            torch.save(model.state_dict(), os.path.join(weights_path, "net_" + str(e) + ".pt"))
            
        #with open(os.path.join(weights_path, "state_" + str(e) + ".txt"), "w") as file:
        #    json.dump(training_variables.__dict__, file, indent = 2)

    print("Training duration:", time.time() - start)
    save_state("final", model, {"lr": optimizer.param_groups[0]["lr"], "epoch": e, "best_epoch": best_epoch, "best_val_accuracy": best_validation_accuracy},
               mask_scheduler, lr_scheduler)
    torch.save(model.state_dict(), os.path.join(weights_path, "net_final.pt"))
#    with open(os.path.join(weights_path, "state_" + str(e) + ".txt"), "w") as file:
#        json.dump(training_variables.__dict__, file, indent = 2)

    if args.tests is not None:
        for i, path in enumerate(test_paths):
            print("Testing dataset:", path)
            test_data = datasets.ActionDataset(path, args.words, transf = f)
    
            val_loss, predictions, ground_truth = test.test(model=model, data=test_data, criterion=criterion, 
                                                                mask_configs=[default_mask], mask_probs=[1], 
                                                                masking = masking, loss = args.loss, k=args.k,
                                                                batchsize = NUM_BATCHES, seq_length= SEQUENCESIZE,
                                                                shuffle = args.shuffle, device = PYTORCH_DEVICE, n_workers=N_WORKERS)
            
            _ = test.evaluation(predictions, ground_truth, test_data.action_perimeters, labels=test_data.words, 
                                sentence_tracking=True, save_file=os.path.join(csv_path, "test_" + str(i)))


    print("Finished training")
